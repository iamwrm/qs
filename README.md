# public_download_scripts

The goal of this repo is to reduce time for bootstrap a new installed linux system by reusing files and scripts.


```
wget  https://gitlab.com/iamwrm/qs/-/raw/main/download_anaconda.sh
```
##

### change mirror

```bash
sed -i 's/deb.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
```

```bash
sudo sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
```


### basic software

```bash
apt update
#apt upgrade -y
apt install -y tmux git zsh curl htop
```
### github mirror
```
git clone
git clone https://ghproxy.com/https://github.com/stilleshan/ServerStatus

wget & curl
wget https://ghproxy.com/https://github.com/stilleshan/ServerStatus/archive/master.zip
wget https://ghproxy.com/https://raw.githubusercontent.com/stilleshan/ServerStatus/master/Dockerfile
curl -O https://ghproxy.com/https://github.com/stilleshan/ServerStatus/archive/master.zip
curl -O https://ghproxy.com/https://raw.githubusercontent.com/stilleshan/ServerStatus/master/Dockerfile
```

### oh my zsh

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

### docker

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
bash get-docker.sh
```

### anaconda

```bash
wget -c https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh
bash Anaconda3-2021.05-Linux-x86_64.sh
```
