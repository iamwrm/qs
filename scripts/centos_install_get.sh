
yum groupinstall "Development Tools"
yum install gettext-devel openssl-devel perl-CPAN perl-devel zlib-devel

# make sure curl is installed so git clone https:// works
sudo yum install curl-devel expat-devel gettext-devel openssl-devel perl-devel zlib-devel asciidoc xmlto docbook2X

wget https://github.com/git/git/archive/v2.33.0.tar.gz -O git.tar.gz

tar -zxf git.tar.gz

cd git-2.33.0

make configure
./configure --prefix=/usr/local
make install

git --version
